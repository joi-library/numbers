/**
 * This package contains interfaces for a system of numbers that is to 
 * replace the default java number system (Integer, Double etc.).
 * This package contain numbers that directly relate to classic number collection
 * theory (TODO REFERENCE HERE!)
 * The most basic number set is the positive number (which includes zero), which is
 * also the only number type that can be interpreted directly.
 * {@link PositiveNumber}
 * {@link WholeNumber}
 * {@link RealNumber}
 * {@link ComplexNumber}
 * 
 * Functions for float/double -> RealNumber?
 * Functions for signed -> WholeNumber?
 */
package com.github.elwinbran.javaoverhaul.numbers;
